import React, { Component } from 'react';
import Loader from "react-loader";
import Button from "react-bootstrap/lib/Button";
import Glyphicon from "react-bootstrap/lib/Glyphicon";

import BootstrapForm from 'react-bootstrap/lib/Form';
import Panel from 'react-bootstrap/lib/Panel';
import { grep, post, save } from "../../api";
import StickyAlert from "../utils/StickyAlert";
import PropTypes from "prop-types";
import { translate } from "react-i18next";

class TokenContainer extends Component {
  static propTypes = {
    t: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      loaded: true,
      saving: false,
      token: "",
      stickyStyle: "success",
      stickyContent: "",
      type: "password"
    };
  }

  componentDidMount() {
  }

  componentWillMount() {
    grep(["patron.privatekey"]).then((keys) => {
      this.setState({ ...this.state, token: keys["patron.privatekey"] });
    });
  }


  render() {
    let stickyContainer;
    const { t } = this.props;
    return (
      <div ref={el => stickyContainer = el}>
        <div className="page-header"><h1>{t("Clé Privée")}</h1></div>

        <Loader loaded={this.state.loaded}>
          <BootstrapForm horizontal>
            <Panel header={<h3>{t('Clé Privée')}</h3>}>
              <input type={this.state.type} id="token"
                     value={this.state.token}
                     onChange={(value) => {
                       this.setState(
                         { ...this.state, token: value.currentTarget.value }
                       );
                     }}/>
              <Button style={{ marginLeft: "1em" }} bsStyle="success"
                      disabled={this.state.saving} onClick={() => {
                        this.setState({ ...this.state, saving: true });
                        save({ "patron.privatekey": this.state.token }).then(() => {
                          this.setState({
                            ...this.state,
                            saving: false,
                            stickyContent: t("La clé a bien été sauvegardée dans votre Recalbox. Emulationstation va se relancer."),
                            stickyStyle: "success"
                          });
                          post("reboot-es", undefined).then(() => {
                            console.error("ok");
                          }, (err) => {
                            console.error(err);
                          });
                        }, () => (
                          this.setState({
                            ...this.state,
                            saving: false,
                            stickyContent: t("Une erreur est survenue lors de la sauvegarde de la configuration."),
                            stickyStyle: "warning"
                          })
                        ));
                      }}><Glyphicon glyph="save"
                            className="glyphicon-save"/> {t("Sauvegarder")}</Button>
              <Button style={{ marginLeft: "1em" }} bsStyle="success"
                      onClick={() => {
                        this.setState({ ...this.state, type: this.state.type === "password" ? "text" : "password" });
                      }}>
                <Glyphicon glyph="show"
                            className={this.state.type === "password" ? "glyphicon-eye-open" : "glyphicon-eye-close"}/> {this.state.type === "password" ? t("Voir") : t("Cacher")}
              </Button>
            </Panel>
          </BootstrapForm>
          <StickyAlert bsStyle={this.state.stickyStyle}
                       container={stickyContainer}>
            {this.state.stickyContent}
          </StickyAlert>
        </Loader>
      </div>
    );
  }
}


export default translate()(TokenContainer);
